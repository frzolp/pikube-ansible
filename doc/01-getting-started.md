# Pi Setup

If you are setting up a cluster completely from scratch without preserving any existing data, this guide is for you. We'll cover the steps involved with imaging the cards from a Linux machine and some pre-boot configuration.

## Hardware needed

* A computer or VM running Linux
* MicroSD cards at least 8 GB in size (I used 64 GB)
* A means of connecting the microSD cards to your Linux host (microSD to USB, microSD to SD, etc)

## Download the Raspbian image

Download the __Raspbian Buster Lite__ image from [raspberrypi.org](https://www.raspberrypi.org/downloads/raspbian/) (Direct link to [latest ZIP](https://downloads.raspberrypi.org/raspbian_lite_latest)). You should end up with a file called ``YYYY-MM-DD-raspbian-buster-lite.zip``, where ``YYYY-MM-DD`` is the image creation date (currently ``2019-07-10``). From your terminal, ``cd`` to the directory you saved the file in and run:

```
$ unzip YYYY-MM-DD-raspbian-buster-lite.zip
```

When the file extraction is complete, you will have a file called ``YYYY-MM-DD-raspbian-buster-lite.img``. We'll be repeating the below steps for each microSD card you'll be using in the cluster.

## Attach the microSD card to the computer

Connect the microSD card to your Linux host. Depending on your setup, your microSD card may appear as ``/dev/mmcblk0``, ``/dev/sdb``, or any number of things. You can find out by running

```
$ sudo fdisk -l
```

Check the output for devices that match the size of your microSD card. On my Dell laptop with an integrated SD card reader, the disk shows up as ``/dev/mmcblk0`` and partitions are ``/dev/mmcblk0p1``, etc. I'll refer to it as ``$SDCARD``.

It may have automatically been mounted when attached to your computer. If it has, unmount it before proceeding.

## Write the image to the disk

Using the device name from before, write the image to the disk with

```
$ sudo dd if=YYYY-MM-DD-raspbian-buster-lite.img of=$SDCARD bs=4M conv=sync,noerror status=progress
```

Once the operation has completed, scan for the new partitions generated on the card with

```
$ sudo partprobe
```

Or just remove the card/reader and re-insert it.

## Mount the boot and root partitions

If your card hasn't been mounted automatically (do a ``mount | grep $SDCARD`` and use the folders listed and skip this step), we'll need to set up mount points for the boot and rootfs partitions.

```
$ sudo mkdir /mnt/pi-boot /mnt/pi-root
```

If your microSD card shows up as a ``/dev/sdX`` device (``/dev/sdb``, etc), the boot partition will be on ``/dev/sdX1`` and the rootfs will be on ``/dev/sdX2``. If it shows up as a ``/dev/mmcblkN`` device (``/dev/mmcblk0``, etc), the boot partition will be on ``/dev/mmcblkNp1`` and rootfs will be ``/dev/mmcblkNp2``. I'll refer to the boot and rootfs partitions as ``$BOOTFS`` and ``$ROOTFS`` accordingly.

```
# mount boot partition, ex. /dev/mmcblk0p1, /dev/sdb1
$ sudo mount $BOOTFS /mnt/pi-boot
# mount rootfs partition, ex. /dev/mmcblk0p2, /dev/sdb2
$ sudo mount $ROOTFS /mnt/pi-root
```

## Enable SSH

We need to create a file in the boot partition called ``ssh``. Raspbian looks for this at startup and if it's present, it enables ``sshd``. To create the file in the boot mount point (I'll call it ``$BOOTDIR``), run

```
$ echo 1 | sudo tee $BOOTDIR/ssh
```

## Set a static IP address on the wired interface

Setting up WiFi is out of scope for this document; we'll be using ethernet to connect these clusters to the network. The rootfs mount point will be called ``$ROOTDIR`` moving forward. First, we'll need to backup the existing ``dhcpcd.conf`` file.

```
$ sudo mv $ROOTDIR/etc/dhcpcd.conf $ROOTDIR/etc/dhcpcd.conf.bak
```

Next, using your favorite editor, create a new ``$ROOTDIR/etc/dhcpcd.conf`` as ``root`` using the below contents as an example:

```
interface eth0
static ip_address=192.168.1.10/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1
```

Make sure to set ``ip_address``, ``routers``, and ``domain_name_servers`` with values appropriate to your network configuration.

Once you are finished, put the microSD cards into the Pis, attach ethernet and power, and wait for the first boot to complete. It will take a little while, since the first boot will resize the rootfs partition. You can attach a display to the HDMI port to monitor its progress, or just ``ping`` its IP address and wait for it to respond.

## First boot

Once your Pi has finished resizing the rootfs partition, rebooted, and is network accessible, SSH into it with the username ``pi`` and the password ``raspberry``. The post-boot tasks will be covered in the next part.
