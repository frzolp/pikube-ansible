# Use f2fs instead of ext4 in rootfs

Raspbian uses [ext4](https://en.wikipedia.org/wiki/Ext4) for the rootfs filesystem. It's a mature and widely-used filesystem, but isn't well optimized for NAND flash-based devices like the microSD cards used by the Raspberry Pi. Since there are intricacies to NAND flash (e.g. wear leveling, TRIM), there are performance and/or reliability gains to be made by using a filesystem that is optimized for that type of storage device. This will go over the steps needed to convert the rootfs partition to [F2FS](https://en.wikipedia.org/wiki/F2FS).

## Install f2fs-tools

Before we even shut off the Raspberry Pi and start messing with partitions, we need to install the package ``f2fs-tools`` on Raspbian **AND** the Linux machine you've been working from (search your distro's package manager in case it's called something else). If the package isn't installed on Raspbian, you won't be able to boot into the f2fs-formatted rootfs because the tools and libraries needed won't be present. You'll need the tools on your Linux machine to create the filesystem and mount it to copy the rootfs data over.

```
$ sudo apt-get install f2fs-tools
```

Once the package is installed, shut down the Pi, remove the microSD card, and attach it to your computer.

## Backup the rootfs contents

If the rootfs isn't automatically mounted, create a mount point for it and mount the device. For example:

```
$ sudo mkdir -p /mnt/rootfs
$ sudo mount /dev/mmcblk0p2 /mnt/rootfs
```

If it's already mounted, note the mount point and use it as the source path instead of ``/mnt/rootfs``. Next, create a directory on the computer that we'll copy the rootfs contents to.

```
$ mkdir $HOME/backup
$ cd backup
$ sudo cp -a /mnt/rootfs/* .
```

The copy can and probably will take several minutes. Once it completes, we'll reformat the rootfs partition.

## Format rootfs as f2fs

This part is simple, just unmount the rootfs partition from ``/mnt/rootfs`` (or wherever it auto-mounted to) and format it as f2fs, substituting the block device name as needed.

```
$ sudo umount /mnt/rootfs
$ sudo mkfs.f2fs -l rootfs -f /dev/mmcblk0p2
```

We're giving the filesystem the label "rootfs" (``-l rootfs``) and because there's an existing filesystem on the partition, we want to force overwrite it (``-f``). This took several minutes on my machine.

## Restore the rootfs contents

Now that the filesystem has been created, we're gonna copy the rootfs files back over to the newly formatted rootfs partition.

```
$ sudo partprobe
$ sudo mount /dev/mmcblk0p2 /mnt/rootfs
$ sudo cp -a $HOME/backup/* /mnt/rootfs/.
```

Again, be patient as this will take a while. Once it finishes, we'll have to make some modifications to a couple files to account for our filesystem change.

## Updating rootfs format type

Mount the ``boot`` partition of the microSD card (if it wasn't automatically) and modify ``cmdline.txt``. Change ``rootfstype=ext4`` to ``rootfstype=f2fs``. For example:

```
$ sed -i 's/ext4/f2fs/g' /mnt/boot/cmdline.txt
$ sudo umount /mnt/boot
```

Next, update the ``fstab`` on the rootfs to use ``f2fs`` instead of ``ext4`` for ``/``. 

The line should look like this:

```
/dev/mmcblk0p2  /      f2fs    defaults,noatime,discard  0       1
```

For example: 

```
$ sudo vim /mnt/rootfs/etc/fstab
$ sudo umount /mnt/rootfs
```

## Boot the Pi

After you've unmounted the card, pop it into the Raspberry Pi and connect network and power. In my experience, it responded to ping a little less than a minute after connecting power for the first boot. Because there's a new disk format, you *may* have to run

```
$ sudo systemctl daemon-reload
```

and reboot again.

Once you've established that your Pi is in working order, delete the backup directory on your computer with 

```
$ sudo rm -rf $HOME/backup
```
