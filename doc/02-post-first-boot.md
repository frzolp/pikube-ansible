# Post-Boot Setup

Now that the Raspberry Pis are up and running, there are a few things we need to change before proceeding with the Ansible playbooks.

## Authentication

### Change the password for the default user

It's a glaring security hole to keep using the ``pi`` user's default password (it's ``raspberry``, remember?), so let's change it using ``passwd``. You'll be prompted to enter the current password and then type in a new password twice to change it.

```
$ passwd
Current password:
New password:
Retype new password:
```

### Add your public SSH key to the list of authorized keys

Pubkey authentication is a nice way to SSH into boxen since you don't need a password (though it can be added as an extra layer of security), which is also what Ansible prefers.

If you have a key already, skip this part and go to *Copying the key starts here*. If not, continue below.

```
$ ssh-keygen -t rsa -b 4096
```

Just hit Enter to accept the default values for everything and you should end up with two files in your ``$HOME/.ssh`` directory: the private key that you **do not share** (``id_rsa``) and the public key that you will use in the next step.

*Copying the key starts here*

The ``pi`` user probably doesn't have ``$HOME/.ssh/authorized_keys``; it wasn't there when I put this together. So let's create it. Repeat these steps on each Raspberry Pi.

```
$ mkdir $HOME/.ssh
$ touch $HOME/.ssh/authorized_keys
$ chmod 600 $HOME/.ssh/authorized_keys
```

On your local machine, open a new terminal and run

```
$ cat $HOME/.ssh/id_rsa.pub | ssh pi@<your_pi_ip> "tee -a .ssh/authorized_keys"
```

You'll be prompted for the ``pi`` user's password and then the contents of your public key will be both displayed on the terminal and written to the ``pi`` user's ``authorized_keys`` file. You should now be able to start a new SSH connection to the Raspberry Pi without having to provide the password.

## Hostname

We want to give each Pi its own unique hostname for several reasons: easy to identify on the network, Kubernetes requires unique hostnames in the cluster, it's good practice, etc. Since we're using static IPs, we'll need to set the hostname on the Pis as well as on the router/DNS server/DHCP server/whatever device handles local DNS on your network.

As long as you're consistent, you can name the Pis whatever you want. I take a more pragmatic approach to naming, so in my two node cluster of one master and one worker, I use this naming scheme:

* Master hostname: ``pikube-master``
* Worker hostname: ``pikube-worker``

If you plan on growing out the cluster, or would like to future-proof your naming scheme, append a two-digit number at the end like so:

* Master 01 hostname: ``pikube-master01``
* Worker 01 hostname: ``pikube-worker01``
* Worker 02 hostname: ``pikube-worker02``

[...]

* Worker 10 hostname: ``pikube-worker10``

Or be creative and name them after Transformers, Sesame Street characters, potent potables, or anything you can imagine!

### Set the hostnames for each pi

On each Pi, run the below command, substituting ``$NEWHOSTNAME`` with the hostname you've chosen for that Pi.

```
$ sudo hostnamectl set-hostname $NEWHOSTNAME
```

Once it completes, reboot the Pi to get the name to take effect

### Create DNS records for the new hostnames

Because there's a million different ways to handle DNS on your network, I can't possibly get into detail on how to create the records for your exact setup. Go to the configuration file/web GUI/command line for your DNS server and create a new record for each Pi, providing the IP address and hostname for each host. 

