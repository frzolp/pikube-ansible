# pikube-ansible

This is an Ansible playbook for creating a two-node Kubernetes cluster (one master, one worker) using Raspberry Pis running Raspbian Buster. More worker nodes can be added by including them in the inventory file. Although this was created with the Raspberry Pi in mind, the steps outlined here are almost entirely compiled from [Kubernetes](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/) and [Docker](https://docs.docker.com/install/linux/docker-ce/debian/) documentation and should be pretty adaptable for any bare-metal Kubernetes deployment.

## Prerequisites

* Two or more Raspberry Pis (tested on Pi 2 and Pi 3) running [Raspbian Buster Lite](https://www.raspberrypi.org/downloads/raspbian/)
	* Enable [SSH access](https://www.raspberrypi.org/documentation/remote-access/ssh/)
	* Configure [static IPs](https://www.raspberrypi.org/forums/viewtopic.php?t=191140#p1200017)
	* Set unique [hostnames](https://geek-university.com/raspberry-pi/change-raspberry-pis-hostname/)
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) on your local machine

## Configuring your environment

Add your public key to the ``pi`` user's ``authorized_keys`` file. Repeat for each node you'll be adding to the cluster.

```
# replace "id_rsa.pub" with the name of your pubkey if it's different.
$ scp ~/.ssh/id_rsa.pub pi@<ip_of_raspberry_pi>:/home/pi/mykey.pub
$ ssh pi@<ip_of_raspberry_pi> "mkdir -p ~/.ssh && cat ~/mykey.pub >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys && rm ~/mykey.pub"
```

Update the Ansible inventory file to point to the hosts you'll be putting in the cluster. Replace ``pikube-master.localdomain`` with the hostname or IP of the host that will be the Kubernetes apiserver, leaving the trailing colon at the end of the line. For worker nodes, replace the ``pikube-worker.localdomain`` line with the hostname or IP of your worker. For additional workers, simply add additional lines under ``pikube-worker.hosts``

```
# for single worker node cluster
#    pikube-worker:
#      hosts:
#        pikube-worker.localdomain:
#
# for multiple worker nodes
#    pikube-worker:
#      hosts:
#        pikube-worker.localdomain:
#        pikube-worker2.localdomain:
#        [...]
$ cd pikube-ansible
$ vim pikube-inventory.yml
```

Ansible needs to know where the inventory is located or else it'll pull from ``/etc/ansible/hosts`` which we don't want.

```
# assuming you're already in the git repo directory (pikube-ansible)
$ export ANSIBLE_INVENTORY=$PWD/pikube-inventory.yml
```

## Run the playbooks

You can either run the master playbook or each playbook in their numbered order. Use ``--check`` at the end to do a dry run (though it will fail from the "configure node ip" stage of ``04-init-cluster.yml`` because it won't perform a prerequisite command).

```
# run everything
$ ansible-playbook 00-everything.yml

# run a single playbook
$ ansible-playbook 01-preinstall.yml
```

## See also

[Kubernetes Setup Using Ansible and Vagrant](https://kubernetes.io/blog/2019/03/15/kubernetes-setup-using-ansible-and-vagrant/)
