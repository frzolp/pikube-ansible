---
- hosts: all
  remote_user: pi
  become: yes
  tasks:

  # Docker is in the Raspbian repos but we're going to use the packages
  # from the official Docker apt repo, so let's make sure we don't have
  # any of the Raspbian packages installed before continuing.
  - name: Remove conflicting packages
    apt:
      autoclean: yes
      autoremove: yes
      state: absent
      name: "{{ packages }}"
    vars:
      packages:
      - docker
      - docker-engine
      - docker.io
      - containerd
      - runc

  # Add the Docker signing key to apt so it can verify the package signatures
  - name: Add docker gpg key
    apt_key:
      url: https://download.docker.com/linux/debian/gpg
      state: present

  # Add the Docker apt repo, getting packages built for armhf (the Pi's CPU architecture)
  - name: Add docker repository
    apt_repository:
      repo: 'deb [arch=armhf] https://download.docker.com/linux/debian buster stable'
      state: present
      filename: docker
      update_cache: yes

  # Now we can install Docker from the official apt repo. We're targeting 18.09.8 because it's
  # the latest version verified by Kubernetes.
  - name: Install docker
    apt:
      name: "{{ packages }}"
      state: present
      install_recommends: no  # otherwise installs aufs-dkms, which is broken on Raspbian Buster!
    vars:
      packages:
      - docker-ce=5:18.09.8~3-0~debian-buster
      - docker-ce-cli=5:18.09.8~3-0~debian-buster
      - containerd.io

  # While upgrading system packages, Docker was also upgraded to a version unsupported by
  # Kubernetes 1.15, so to avoid this we'll put a hold on the Docker packages.
  - name: Hold the Docker packages to prevent unexpected upgrades
    shell: apt-mark hold docker-ce docker-ce-cli

  # The daemon.json file has some parameters set for how Docker should run, like using the systemd
  # cgroup driver and logging rotation.
  - name: Copy Docker daemon.json
    copy:
      src: files/daemon.json
      dest: /etc/docker/daemon.json

  # Free up space by getting rid of old and unused packages.
  - name: Remove unneeded and old packages
    apt:
      autoclean: yes
      autoremove: yes

  # For the sake of simplicity, add the pi user to the "docker" group to give it permission to
  # interact with the Docker daemon (i.e. "docker run", "docker exec", "docker stop").
  - name: Add pi user to docker group
    user:
      name: pi
      groups: docker
      append: yes

  # Make sure the Docker daemon is set to run at boot and restart it to load the settings in
  # the daemon.json file.
  - name: Start docker service if not already
    service:
      name: docker
      enabled: yes
      state: restarted
